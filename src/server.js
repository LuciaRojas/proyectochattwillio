const express= require('express');
const app = express();
const morgan =require('morgan');
const path=require('path');
const exphbs = require('express-handlebars'); // configurar el motor de plantillas
const session = require('express-session');
const bodyParser = require("body-parser");
const { getSocket } = require("../src/sockets");
//const { getSocket } = require("../sockets");




const {
  base64decode
} = require('nodejs-base64');
const credential= require('../src/models/credentials');
//settings
const {
  login,
    getChannels,
    sendlogin,
    CreateChannel,
    DeleteChannel,
    loginInvitado,
    mainInvitado,
    UpdateChannel,
    addChannel,
    editChannel,
    CreateMemberRe,
    CreateMessage,
    actualizarMessages,
  } = require("./Controllers/twControllers");
 


app.set('port', process.env.PORT || 3000 );
app.set('views',path.join( __dirname, 'views'));
app.engine('.hbs',exphbs({
    layoutsDir:path.join(app.get('views'),'layouts'),
    partialsDir: path.join(app.get('views'),'partials'),
    defaultLayout:'main',
    extname: '.hbs'
}));
app.set('view engine', '.hbs');

//middlewares
app.use(morgan('dev'));
app.use(express.json());
app.use(bodyParser.urlencoded({
  extended: true,
}));
app.use(express.urlencoded({extended:false}));
app.use(session({secret: 'lucy2'}));


// parses the body
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(bodyParser.json());

// check for cors
const cors = require("cors");
app.use(cors({
  domains: '*',
  methods: "*"
}));


//routes
//app.use(require('./routes/indexroutes'));

/**Funcion para guardar los datos en la seccion */
app.use(function (req, res, next) {
      req.session.username=credential.user;
      req.session.password= credential.password;
    if (req.session.username === 'lucy' &&  req.session.password == '2025') {
      // saveSession('admin');
      next();
      return;
    }
  
  res.status(401);
  res.send({
    error: "Unauthorized"
  });
});

// handle the routes
//get
 app.get("/", login);
 app.get("/loginInvitado", loginInvitado);
 app.get("/getChannels", getChannels);
 app.get("/addChannel",addChannel);
//post
 app.post("/send-login", sendlogin);
 app.get("/mainInvitado", mainInvitado);
app.post("/CreateChannel", CreateChannel);
app.post("/CreateMessage", CreateMessage);

app.get("/actualizarMessages/:ChannelSid/:name_user", actualizarMessages);
//update
app.get("/editChannel/:sid/:friendly_name", editChannel);
app.post("/UpdateChannel", UpdateChannel);
app.post("/DeleteChannel", DeleteChannel);
app.delete("/DeleteChannel", DeleteChannel);
app.post("/CreateMemberRe", CreateMemberRe);
//delete
//app.delete("/DeleteChannel/:sid", DeleteChannel);
/*app.patch("/api/tasks", patchMethod);
app.put("/api/tasks", patchMethod);*/




//static files
app.use(express.static(path.join(__dirname,'public')));
//app.get('/',(req, res)=> {
//    res.send('hello world')
//}) 


module.exports = app;