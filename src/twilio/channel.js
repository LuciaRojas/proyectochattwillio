const { json } = require('express');
const fetch = require('node-fetch');


const credential= require('../models/credentials');

//const client=require('twilio')(config.accountSid,config.authToken);
/**
 * 
 * @param {string} body -The body message
 * @param {string} phone  the number phone
 */

//const studentPost = (req, res) => {
 const    getServices= async(req,res) => {
  var url ='https://chat.twilio.com/v2/Services/';
  var headers = await {
    'Authorization': 'Basic ' + Buffer.from(credential.accountSid+":"+credential.authtoken).toString('base64'),
    'Content-Type': 'application/json; charset=utf-8',
    'dataType': 'json',
  }
  
  //console.log(headers);
  
  fetch(url, { method: 'GET', headers: headers})
    .then((res) => {
       return res.json();
  })
  .then((dato) => {
    //console.log(dato);
    
    return res.json(dato);
    //res.send(json);
    // Do something with the returned data.
  });
}

function getChannels(res){
 // https://chat.twilio.com/v2/Services/{ServiceSid}/Channels
  var url= 'https://chat.twilio.com/v2/Services/'+credential.serviceSid+'/Channels';
  var headers = {
    'Authorization': 'Basic ' + Buffer.from(credential.accountSid+":"+credential.authtoken).toString('base64'),
    'Content-Type': 'application/json; charset=utf-8'
  }
  
  console.log(headers);
  
  fetch(url, { method: 'GET', headers: headers})
    .then((res) => {
       return res.json()
  })
  .then((json) => {
    console.log(json);
    res.json(json);
    res.send(json);
    // Do something with the returned data.
  });
}

/***
 * 
 */
function createChannels(res){
  var url= 'https://chat.twilio.com/v2/Services/'+credential.serviceSid+'/Channels';

  req.params.name_channle;
  console.log(url);
  var headers = {
    'Authorization': 'Basic ' + Buffer.from(credential.accountSid+":"+credential.authtoken).toString('base64'),
    'Content-Type': 'application/json; charset=utf-8'
  }
  fetch(url, { method: 'POST', headers: headers})
                .then((res) => {
                   return res.json()
              })
              .then((dato) => {
                console.log(dato);
               res.render('index',{dato});
                // Do something with the returned data.
              });
}





module.exports={
   getServices,
  getChannels,
  createChannels
};