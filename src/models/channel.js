const {Schema, model}= require('mongoose');

const newSchema = new Schema({
    friendlyname:{
        type:String,
        require:true
    },
    sid: {
        type:String
    }
},{
    timestamps:true
});

module.exports= model('sms', newSchema);