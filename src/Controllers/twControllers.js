const credential= require('../models/credentials');
const fetch = require('node-fetch');
var axios = require('axios');
var FormData = require('form-data');
const { Message } = require('twilio/lib/twiml/MessagingResponse');
const { getSocket } = require("../sockets");

var headers = {
  'Authorization': 'Basic ' + Buffer.from(credential.accountSid+":"+credential.authtoken).toString('base64'),
  'Content-Type': 'application/json'
};

/**
 * login lo renderisa a la pagina de login
 * @param {*} req 
 * @param {*} res 
 */

const login = (req, res) => {
    res.render('login');
};

/**
 * redirecciona al formulario de agregar channel
 * @param {string } req 
 * @param {*} res 
 */
const addChannel = (req, res) => {
 
  res.render('addChannel');
 // res.redirect();
};
/**
 * Obtiene todos los channels existentes y los redirecciona 
 * al login nuevamente y si es correcto entra el index de administrador
 */
const getChannels = async (req, res) => {           ///IS26be93565ec44cb5aab62eb2d5e14b38   
    var url= 'https://chat.twilio.com/v2/Services/'+credential.serviceSid+'/Channels';
   var config= {
              method: 'get',
              url: url,
              headers: headers,
          };
          await axios(config).then(function(response) {
           let dato=response.data;
           console.log(dato);
      // console.log(JSON.stringify(channels));  
           res.render('index',{dato});
    
          })
              .catch(function(error) {
                   console.log(error);
              });

};

/**
 * 
  Valida que los username y password sean los mismos que  req.session.username y req.session.password
  * @param {string } req 
 * @param {*} res 
 */
const sendlogin = (req, res) => {
   
    const {usern,passw}=req.body;

    if (!usern || !passw) {
     var message ="Write a username or password"
     res.render('login',{message});
    }else{
        if(req.session.username==usern || req.session.password ==passw){
          getChannels(req, res);
          
        }
    }
};

/**
 * Crea los channels apartir del sid del srevicio
  * @param {string } req 
 * @param {*} res 
 */
const CreateChannel = (req, res) => {
   // const formData = new FormData();
    const {name_channel}=req.body;
      if(!name_channel){
        var message ="Write name Channel"
        res.render('addChannel',{message});

      }else{
        console.log(name_channel);
        var url= 'https://chat.twilio.com/v2/Services/'+credential.serviceSid+'/Channels';
        var data = new FormData();
    
      data.append('FriendlyName', name_channel);
      var headers = {
        'Authorization': 'Basic ' + Buffer.from(credential.accountSid+":"+credential.authtoken).toString('base64'),
        ...data.getHeaders()
    
      };
      var config = {
        method: 'post',
        url: url,
        headers: headers,
        data : data
      };
    
    axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      var message="se creo el channel";
      res.render('addChannel',{message});
    })
    .catch(function (error) {
      console.log(error);
    });

      }
      
};
/**
 * redirecciona para que pueda editar el channel
 * @param {string } req 
 * @param {*} res 
 */
  const editChannel = (req, res) => {
  var id_channel=req.params.sid;
  var name_channel= req.params.friendly_name;
      
      res.render('editeChannel',{id_channel,name_channel});
    };

/**
 * actualiza el channel y se cambia el nombre del del channel
 * @param {string } req 
 * @param {*} res 
 */
const UpdateChannel = (req, res) => {
  const {name_channel,id_channel}=req.body;

  if(!name_channel){
    var message ="Write name Channel"
    res.render('editeChannel',{message});

  }else{
    https://chat.twilio.com/v2/Services/{ServiceSid}/Channels/{Sid}
    var url= 'https://chat.twilio.com/v2/Services/'+credential.serviceSid+'/Channels/'+id_channel+'';
    var data = new FormData();

  data.append('FriendlyName', name_channel);
  var headers = {
    'Authorization': 'Basic ' + Buffer.from(credential.accountSid+":"+credential.authtoken).toString('base64'),
    ...data.getHeaders()

  };
  var config = {
    method: 'post',
    url: url,
    headers: headers,
    data : data
  };

axios(config)
.then(function (response) {
  console.log(JSON.stringify(response.data));
  
  var message ="Se actualizo el channel"

  res.render('editeChannel',{name_channel,id_channel,message});
})
.catch(function (error) {
  console.log(error);
});


  }
 };
            
const loginInvitado = (req, res) => {
    res.render('loginInvitado');
}
/**
 * pagina principal que ve el invitado
 * @param {*} req user del invitado
 * @param {*} res 
 */
const mainInvitado = (req, res) => {
    getChannelsInvitado(req, res);
    //res.render('mainInvitado',{nameusuario});
  
};
/***
 * obtiene todos los channel del service y los redireccional al mainInvitado
 * @param {string } req 
 * @param {*} res 
 */

const getChannelsInvitado = async (req, res) => {
  var url= 'https://chat.twilio.com/v2/Services/'+credential.serviceSid+'/Channels';
      
      var config= {
              method: 'get',
              url: url,
              headers: headers,
          };
          await axios(config).then(function(response) {
           let dato=response.data;
           console.log(dato);
      // console.log(JSON.stringify(channels));  
           res.render('mainInvitado',{dato}); 
    
          })
              .catch(function(error) {
                   console.log(error);
              });

};

/***  
 * Elimina el channel apartir del sid del servicio y con sid del channel
 * @param {string } req 
 * @param {*} res 
 */
const DeleteChannel = async (req, res) => {
  const sid_channel  = req.params.sid;
  const {ChannelSid}=req.body;
  console.log("entro");
//const {sid_channel}=req.body;
//if (!name_channel) return ('Debes ingresar un nombre');

console.log(ChannelSid);
 var url= 'https://chat.twilio.com/v2/Services/'+credential.serviceSid+'/Channels/'+ChannelSid+'';
  var data=new FormData();
data.append('sid', ChannelSid);
var headers = {
  'Authorization': 'Basic ' + Buffer.from(credential.accountSid+":"+credential.authtoken).toString('base64'),
  ...data.getHeaders()

            };

          var config= {
              method: 'delete',
              url: url,
              headers: headers,
              data : data
                    };
              
                    await   axios(config)
                  .then(function (response) {
              console.log(JSON.stringify(response.data));
                  })

                  getChannels(req, res);

};

/***
 * obtiene todos los channel del service y los redireccional al mainInvitado
 * id del channel
 * el nombre del channel
 * @param {string } req 
 * @param {*} res 
 */
const getChannelsMessage = async (req, res) => {
 const {name_user,ChannelSid}=req.body;

 //console.log(ChannelSid);
  var url= 'https://chat.twilio.com/v2/Services/'+credential.serviceSid+'/Channels/'+ChannelSid+'/Messages';   
   
      var config= {
        method: 'get',
        url: url,
        headers: headers,
    };
    await axios(config).then(function(response) {
    let messages= response.data;
 
      getChannelsMemebers(req, res,messages);
    
    return response.data;
    })
.catch(function(error) {
if (error.response.status == 409) {
      console.log("Message");
}
else {
     console.log(error);
}
 });
};



/****
 * obtiene todos los chanels de los miembros de cjannel
 ** @param {*} req 
 * @param {*} res 
 * @param {*} messages datos del getChannelsMessage
 */
const getChannelsMemebers = (req, res,messages) => {

   const {name_user,ChannelSid}=req.body;

//https://chat.twilio.com/v2/Services/{ServiceSid}/Channels/{ChannelSid}/Members
  var url= 'https://chat.twilio.com/v2/Services/'+credential.serviceSid+'/Channels/'+ChannelSid+'/Members';  
   

  var config= {
  method: 'get',
  url: url,
  headers: headers,
  };
   axios(config).then(function(response) {
 let members=response.data;
//console.log(JSON.stringify(channels)); 
//getSocket().emit('new message',members);
res.render('Channel',{ChannelSid,name_user,messages,members});
})
.catch(function(error) {
if (error.response.status == 409) {
      console.log("M");
}
else {
     console.log(error);
}
 });
};

/**
 * crea los miembros  del canal 
 * @param {*} req 
 * @param {*} res 
 */
const addMemberRe = async(req, res) => {
const {name_user,ChannelSid}=req.body;
  var url= 'https://chat.twilio.com/v2/Services/'+credential.serviceSid+'/Channels/'+ChannelSid+'/Members';
 // https://chat.twilio.com/v2/Services/{ServiceSid}/Channels/{ChannelSid}/Members
  var data = new FormData();
  data.append('Identity', name_user);
  var headers = {
    'Authorization': 'Basic ' + Buffer.from(credential.accountSid+":"+credential.authtoken).toString('base64'),
    ...data.getHeaders()
  }
 
  var config = {
    method: 'post',
    url: url,
    headers: headers,
    data : data
  };

await axios(config)
.then(function (response) {
  //console.log(JSON.stringify(response.data));

})
.catch(function (error) {
 if (error.response.status == 409) {
      console.log("Usuario Existe");
  }
  else {
     console.log(error);
 }
});



};
/**
 * crea los mensajes del channel
 * @param {string } req 
 * @param {*} res 
 */
const addMessage = async(req, res) => {
  const {message,ChannelSid,name_user}=req.body;
  var url= 'https://chat.twilio.com/v2/Services/'+credential.serviceSid+'/Channels/'+ChannelSid+'/Messages';
 //https://chat.twilio.com/v2/Services/{ServiceSid}/Channels/{ChannelSid}/Messages
  var data = new FormData();

  data.append('Body', message);
  data.append('From', name_user);
  var headers = {
    'Authorization': 'Basic ' + Buffer.from(credential.accountSid+":"+credential.authtoken).toString('base64'),
    ...data.getHeaders()
  }
  var config = {
    method: 'post',
    url: url,
    headers: headers,
    data : data
  };

  await axios(config)
    .then(function (response) {
     // console.log(JSON.stringify(response.data));
     })
};

/***
 * crea el miembro en el grupo
  * @param {string } req 
 * @param {*} res 
 */
const CreateMemberRe = (req, res) => {
const {ChannelSid,name_user}=req.body;
        addMemberRe(req, res);
    getChannelsMessage(req, res);
     
};

/**
 * crea el mensaje y luego obtiene los datos de los mensajesmessages 
 * @param {string } req 
 * @param {*} res 
 */
const CreateMessage = async(req, res) => {
  const {ChannelSid,name_user}=req.body;
    addMessage(req, res);
     getChannelsMessage(req, res);

    
};

const actualizarMessages = async(req, res) => {
  var ChannelSid;
  var name_user;

  if(req.body.ChannelSid){
    ChannelSid=req.body.ChannelSid;
    name_user= req.body.name_user;
  }else{

    ChannelSid=req.params.ChannelSid;
    name_user= req.params.name_user;
  }
console.log(ChannelSid);
console.log(name_user);

console.log("hols");

  getChannelsMessage2(req, res,ChannelSid,name_user);
    
};


const getChannelsMessage2 = async (req, res,ChannelSid,name_user) => {

 
  //console.log(ChannelSid);
   var url= 'https://chat.twilio.com/v2/Services/'+credential.serviceSid+'/Channels/'+ChannelSid+'/Messages';   
    
       var config= {
         method: 'get',
         url: url,
         headers: headers,
     };
     await axios(config).then(function(response) {
     let messages= response.data;
  
       getChannelsMemebers2(req, res,messages,ChannelSid,name_user);
     
     return response.data;
     })
 .catch(function(error) {
 if (error.response.status == 409) {
       console.log("Message");
 }
 else {
      console.log(error);
 }
  });
 };
 
 
 
 /****
  * obtiene todos los chanels de los miembros de cjannel
  ** @param {*} req 
  * @param {*} res 
  * @param {*} messages datos del getChannelsMessage
  */
 const getChannelsMemebers2 = (req, res,messages,ChannelSid,name_user) => {
 
 
 //https://chat.twilio.com/v2/Services/{ServiceSid}/Channels/{ChannelSid}/Members
   var url= 'https://chat.twilio.com/v2/Services/'+credential.serviceSid+'/Channels/'+ChannelSid+'/Members';  
    
 
   var config= {
   method: 'get',
   url: url,
   headers: headers,
   };
    axios(config).then(function(response) {
  let members=response.data;
 //console.log(JSON.stringify(channels)); 
 //getSocket().emit('new message',members);
 res.render('Channel',{ChannelSid,name_user,messages,members});
 })
 .catch(function(error) {
 if (error.response.status == 409) {
       console.log("M");
 }
 else {
      console.log(error);
 }
  });
 };
 



module.exports = {
    login,
    getChannels,
    sendlogin,
    CreateChannel,
    DeleteChannel,
    loginInvitado,
    mainInvitado,
    UpdateChannel,
    addChannel,
    editChannel,
    CreateMemberRe,
    CreateMessage,
    actualizarMessages
  }